import React, {useEffect, useState} from "react";
import fetch from 'node-fetch';
import {
  DataGridPremium,
  GridToolbar,
  useGridApiRef,
  useKeepGroupedColumnsHidden,
} from '@mui/x-data-grid-premium';
import Box from '@mui/material/Box';
import {createTheme, ThemeProvider} from "@mui/material";
import Links from "../components/Links";
import {myFont} from "../components/myFont";


function getTheme(mode){
    return (mode === "light") ? (createTheme({

        palette: {
            type: 'light',
            primary: {
                main: "#O00000",
            },
            secondary: {
                main: "#000000",
            },
        },
        typography: {
            fontFamily: myFont,
            h1:{
                fontFamily: myFont
            },
        },

    })): createTheme({
        palette: {
            mode: 'dark',
            primary: {
                main: "#FFFFFF",
            },
            secondary: {
                main: "#FFFFFF",
            },
        },
        typography: {
            fontFamily: myFont,
            h1: {
                fontFamily: myFont
            }
        }
    })
}



export async function getStaticProps(){

    const payments = await fetch("https://undefxx.com/api/p", {method: "GET", headers: { propertyID : process.env.NEXT_PUBLIC_PROPERTY_ID}}).then(x=> x.json());

    return{
        props: {payments},
        revalidate: 1
    }
}


export default function Log(props){
    const unpaid =  props.payments[process.env.NEXT_PUBLIC_PROPERTY_ID].unpaid
    const processing =  props.payments[process.env.NEXT_PUBLIC_PROPERTY_ID].processing
    const paid =  props.payments[process.env.NEXT_PUBLIC_PROPERTY_ID].paid
    const logData = {... processing, ... paid }
    const [theme, setTheme] = useState(getTheme)

    useEffect(()=> {
        window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', event => {
            if(theme === "dark"){
                setTheme("light")
            } else setTheme("dark")
        });
    },[])

    function getCurrentTheme(){
        if (typeof window === "undefined") {
            return "light"
        }
        if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
            return "dark"
        }
        else return "light"
    }

    let links = [{label: "<---", href: "/"}, {label: "", href: "/"}]

    for(let elem in unpaid) {
        links.push({label: "", href: '/'})
    }
    links.push({label: "...", href: '/'})

    const [rows, setRows] = useState(getRows(logData))
    const [columns, setColumns] = useState([{field: "name", headerName:"id", width: 100}, {field: "status", headerName:"status", width: 100},  {field: "total", headerName: "total", width: 100, type: "number"}])


    return(
            <div className={myFont.className}>
             <Links links = {links}/>
            <div className={"w-96 content-center mx-auto"}>
                <ThemeProvider theme={getTheme(getCurrentTheme())}>
                <Box  sx={{height: 540}}>
                     <DataGridPremium  rows ={rows} columns ={columns} components={{ Toolbar: GridToolbar }} experimentalFeatures={{ aggregation: true }}/>
                </Box>
                </ThemeProvider>
            </div>
            </div>
    )
}

function getRows(data) {
    let rows = []
    for (let key in data){
        if (data[key].status == "paid") {
            data[key].status = "complete"
        } else if (data[key].status == "processing") {
            data[key].status = "in progress"
        }
        rows.push({id: key, name: data[key].name,total: data[key].amount, status: data[key].status })
    }
    return rows
}